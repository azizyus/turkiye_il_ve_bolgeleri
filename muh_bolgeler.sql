-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 01 Ağu 2017, 19:27:00
-- Sunucu sürümü: 10.1.9-MariaDB
-- PHP Sürümü: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `gezginol`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `muh_bolgeler`
--

CREATE TABLE `muh_bolgeler` (
  `id` int(11) NOT NULL,
  `baslik` varchar(255) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `muh_bolgeler`
--

INSERT INTO `muh_bolgeler` (`id`, `baslik`) VALUES
(1, 'Akdeniz Bölgesi\n'),
(2, 'Ege Bölgesi'),
(3, 'Marmara Bölgesi'),
(4, 'Karadeniz Bölgesi'),
(5, 'İç Anadolu Bölgesi'),
(6, 'Doğu Anadolu Bölgesi'),
(7, 'Güney Doğu Anadolu Bölgesi');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `muh_bolgeler`
--
ALTER TABLE `muh_bolgeler`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `muh_bolgeler`
--
ALTER TABLE `muh_bolgeler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
