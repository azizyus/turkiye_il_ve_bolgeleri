-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 01 Ağu 2017, 19:23:54
-- Sunucu sürümü: 10.1.9-MariaDB
-- PHP Sürümü: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `gezginol`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `muh_iller`
--

CREATE TABLE `muh_iller` (
  `id` int(11) NOT NULL,
  `baslik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bolge` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `muh_iller`
--

INSERT INTO `muh_iller` (`id`, `baslik`, `bolge`) VALUES
(1, 'Adana', '1'),
(2, 'Adıyaman', '7'),
(3, 'Afyonkarahisar', '2'),
(4, 'Ağrı', '6'),
(5, 'Amasya', '4'),
(6, 'Ankara', '5'),
(7, 'Antalya', '1'),
(8, 'Artvin', '4'),
(9, 'Aydın', '2'),
(10, 'Balıkesir', '3'),
(11, 'Bilecik', '3'),
(12, 'Bingöl', '6'),
(13, 'Bitlis', '6'),
(14, 'Bolu', '4'),
(15, 'Burdur', '1'),
(16, 'Bursa', '3'),
(17, 'Çanakkale', '3'),
(18, 'Çankırı', '5'),
(19, 'Çorum', '4'),
(20, 'Denizli', '2'),
(21, 'Diyarbakır', '7'),
(22, 'Edirne', '3'),
(23, 'Elazığ', '6'),
(24, 'Erzincan', '6'),
(25, 'Erzurum', '6'),
(26, 'Eskişehir', '5'),
(27, 'Gaziantep', '7'),
(28, 'Giresun', '4'),
(29, 'Gümüşhane', '4'),
(30, 'Hakkari', '6'),
(31, 'Hatay', '1'),
(32, 'Isparta', '1'),
(33, 'Mersin', '1'),
(34, 'İstanbul', '3'),
(35, 'İzmir', '2'),
(36, 'Kars', '6'),
(37, 'Kastamonu', '4'),
(38, 'Kayseri', '5'),
(39, 'Kırklareli', '3'),
(40, 'Kırşehir', '5'),
(41, 'Kocaeli', '3'),
(42, 'Konya', '5'),
(43, 'Kütahya', '2'),
(44, 'Malatya', '6'),
(45, 'Manisa', '2'),
(46, 'Kahramanmaraş', '1'),
(47, 'Mardin', '7'),
(48, 'Muğla', '2'),
(49, 'Muş', '6'),
(50, 'Nevşehir', '5'),
(51, 'Niğde', '5'),
(52, 'Ordu', '4'),
(53, 'Rize', '4'),
(54, 'Sakarya', '3'),
(55, 'Samsun', '4'),
(56, 'Siirt', '7'),
(57, 'Sinop', '4'),
(58, 'Sivas', '5'),
(59, 'Tekirdağ', '3'),
(60, 'Tokat', '4'),
(61, 'Trabzon', '4'),
(62, 'Tunceli', '6'),
(63, 'Şanlıurfa', '7'),
(64, 'Uşak', '2'),
(65, 'Van', '6'),
(66, 'Yozgat', '5'),
(67, 'Zonguldak', '4'),
(68, 'Aksaray', '5'),
(69, 'Bayburt', '4'),
(70, 'Karaman', '5'),
(71, 'Kırıkkale', '5'),
(72, 'Batman', '7'),
(73, 'Şırnak', '7'),
(74, 'Bartın', '4'),
(75, 'Ardahan', '6'),
(76, 'Iğdır', '6'),
(77, 'Yalova', '3'),
(78, 'Karabük', '4'),
(79, 'Kilis', '7'),
(80, 'Osmaniye', '1'),
(81, 'Düzce', '4');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `muh_iller`
--
ALTER TABLE `muh_iller`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `muh_iller`
--
ALTER TABLE `muh_iller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
